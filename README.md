# Security Analysis of WPA3

This repo is directly about using one of the dragonblood vulnerabilities
to exploit WPA3.

# Background

Background on WEP and WPA1, WPA2, and WPA3 is
[here](https://borisnikulin.gitlab.io/security-wpa2-wpa3/background.pdf).

Background on dragonblood is
[here](https://borisnikulin.gitlab.io/security-wpa2-wpa3/dragonblood.pdf).
